#include "BSP.h"

#include <algorithm>
#include <limits>
#include <stdexcept>

#include <glm/gtx/norm.hpp>

#include "Brush.h"
#include "Node.h"

namespace bsp {
    namespace internal {
        std::vector<Plane> BuildBoundingPlanes(const glm::vec3 &bbox_min, const glm::vec3 &bbox_max);
        std::vector<Poly> BuildBoundingPolys(VertexPool *pool, const glm::vec3 &min, const glm::vec3 &max);

        int ChooseDividingPoly(const std::vector<Poly> &polys, const std::vector<Plane> &planes,
                               const std::vector<Poly> &prev_dividers, float eps);

        std::unique_ptr<Node> BuildTree(std::vector<Poly> &polys, std::vector<Plane> &planes, float eps);

        void GeneratePortals(Node *root, VertexPool *pool, float eps);
        
        void VisualizeTree(Node *root);
    }
}


std::unique_ptr<bsp::Node> bsp::BuildFromBrushes(const std::vector<Brush> &clipped_brushes, VertexPool *pool) {
    const float eps = pool->eps();
    
    auto set_poly_index = [](Poly &poly, unsigned index) {
        // use last 16 bits of flags as index
        assert(index < std::numeric_limits<uint16_t>::max());
        uint16_t f = (uint16_t)index;
        uint32_t flags = poly.flags();
        uint16_t *pf = (uint16_t *)&flags;
        pf[1] = f;
        poly.set_flags(flags);
    };

    auto get_poly_index = [](const Poly &poly) -> uint16_t {
        uint32_t flags = poly.flags();
        uint16_t *pf = (uint16_t *)&flags;
        return pf[1];
    };
    
    glm::vec3 bbox_min = glm::vec3{ std::numeric_limits<float>::max() },
              bbox_max = glm::vec3{ -std::numeric_limits<float>::max() };	      

    {   // find bounding box
        const float BOUNDING_OFFSET = 10;

        for (auto &b : clipped_brushes) {
            unsigned num_sides = b.num_sides();
            for (unsigned i = 0; i < num_sides; i++) {
                unsigned num_points = b.poly(i).num_points();
                for (unsigned p = 0; p < num_points; p++) {
                    const glm::vec3 &pos = b.poly(i).vtx_pos(p);
		
                    bbox_min = glm::min(bbox_min, pos);
                    bbox_max = glm::max(bbox_max, pos);
                }
            }
        }

        bbox_min -= glm::vec3(BOUNDING_OFFSET);
        bbox_max += glm::vec3(BOUNDING_OFFSET);
    }

    enum ePolyFlags { F_NOT_LEGAL = 1 };
    
    // build bounding polygons around scene
    std::vector<Plane> outside_planes = internal::BuildBoundingPlanes(bbox_min, bbox_max);
    std::vector<Poly> outside_polys = internal::BuildBoundingPolys(pool, bbox_min, bbox_max);

    // mark them as not legal to delete all zones connected to them later
    for (auto &p : outside_polys) {
        p.set_flags(F_NOT_LEGAL);
    }

    std::vector<Plane> total_planes;
    std::vector<Poly> total_polys;

    total_planes.insert(total_planes.end(), outside_planes.begin(), outside_planes.end());
    total_polys.insert(total_polys.end(), outside_polys.begin(), outside_polys.end());

    for (auto &b : clipped_brushes) {
        unsigned num_sides = b.num_sides();
        for (unsigned i = 0; i < num_sides; i++) {
            total_planes.push_back(b.plane(i));
            total_polys.push_back(b.poly(i));
            set_poly_index(total_polys.back(), (unsigned)(total_polys.size() - 1));
        }
    }

    {   // early check for errors
        for (auto &p : total_polys) {
            if (p.num_points() < 3) {
                throw std::runtime_error("Invalid polygon found!");
            }

            //printf("poly %i/%i\n", int(&p - &total_polys[0]), int(total_polys.size()));
            for (unsigned i = 0; i < p.num_points() - 1; i++) {
                //printf("%f %f %f | %f %f %f\n", p.vtx_pos(i).x, p.vtx_pos(i).y, p.vtx_pos(i).z,
                //p.vtx_pos(i + 1).x, p.vtx_pos(i + 1).y, p.vtx_pos(i + 1).z);
                if (glm::distance2(p.vtx_pos(i), p.vtx_pos(i + 1)) < eps * eps) {
                    throw std::runtime_error("Merged points found!");
                }
            }
        }
    }

    std::unique_ptr<Node> root;

    {   // build tree first time to find unreachable polygons
        root = internal::BuildTree(total_polys, total_planes, eps);
        internal::GeneratePortals(root.get(), pool, eps);
    }

    internal::VisualizeTree(root.get());
    
    {   // delete nodes connected to marked as not legal
        auto delete_leaf = [](Node *node) {
            assert(node->has_polys());
            Node *parent = node->parent_node();
            if (parent->front_node() == node) {
                parent->set_front_node(nullptr);
            } else {
                parent->set_back_node(nullptr);
            }
        };

        auto delete_non_legal_leafs = [delete_leaf](Node *node) {
            if (node->has_polys() && node->QueryPolyFlags(F_NOT_LEGAL)) {
                node->ExecForConnected(delete_leaf);
            }
        };

        root->Exec(delete_non_legal_leafs);
    }
    
    internal::VisualizeTree(root.get());

    {   // delete unreachable polygons
        std::vector<unsigned> poly_indices;
        auto gather_polys = [&poly_indices, get_poly_index](Node *node) {
            for (const auto &p : node->polys()) {
                unsigned i = get_poly_index(p);
                if (std::find(poly_indices.begin(), poly_indices.end(), i) == poly_indices.end()) {
                    poly_indices.push_back(i);
                }
            }
        };

        root->Exec(gather_polys);
        
        for (unsigned i = 0; i < total_polys.size(); ) {
            if (std::find(poly_indices.begin(), poly_indices.end(),
                          get_poly_index(total_polys[i])) == poly_indices.end()) {
                total_polys.erase(total_polys.begin() + i);
                total_planes.erase(total_planes.begin() + i);
            } else {
                i++;
            }
        }
    }

    printf("Polys after cleanup: %i\n", (int)total_polys.size());
    
    {   // build tree again
        root.reset();
        root = internal::BuildTree(total_polys, total_planes, eps);
        internal::GeneratePortals(root.get(), pool, eps);
    }

    {   // build pvs with help of portals
        auto build_pvs = [eps](Node *node) {
            node->BuildPVS(eps);
        };

        root->Exec(build_pvs);
    }

    internal::VisualizeTree(root.get());

    {   // project polys to closest axis plane to create texture coordinates
        const float TEXTURE_SCALE = 1.0f/512;
        auto update_uvs = [TEXTURE_SCALE](Node *node) {
            node->UpdateUVs(TEXTURE_SCALE);
        };

        root->Exec(update_uvs);
    }

    return root;
}



void bsp::internal::GeneratePortals(Node *root, VertexPool *pool, float eps) {
    auto generate_portals = [&pool, eps](Node *node) {
        if (!node->has_polys()) {
            node->GeneratePortals(pool, eps);
        }
    };

    root->Exec(generate_portals);
}

std::vector<bsp::Plane> bsp::internal::BuildBoundingPlanes(const glm::vec3 &bbox_min, const glm::vec3 &bbox_max) {
    return {{{1, 0, 0}, -bbox_min[0]},
            {{-1, 0, 0}, bbox_max[0]},
            {{0, 1, 0}, -bbox_min[1]},
            {{0, -1, 0}, bbox_max[1]},
            {{0, 0, 1}, -bbox_min[2]},
            {{0, 0, -1}, bbox_max[2]}};
}

std::vector<bsp::Poly> bsp::internal::BuildBoundingPolys(VertexPool *pool, const glm::vec3 &bbox_min, const glm::vec3 &bbox_max) {
    std::vector<Poly> out_polys(6, pool);
    
    out_polys[0].AddVertex(bbox_min, {}, {});
    out_polys[0].AddVertex({bbox_min.x, bbox_max.y, bbox_min.z}, {}, {});
    out_polys[0].AddVertex({bbox_min.x, bbox_max.y, bbox_max.z}, {}, {});
    out_polys[0].AddVertex({bbox_min.x, bbox_min.y, bbox_max.z}, {}, {});

    out_polys[1].AddVertex({bbox_max.x, bbox_min.y, bbox_min.z}, {}, {});
    out_polys[1].AddVertex({bbox_max.x, bbox_min.y, bbox_max.z}, {}, {});
    out_polys[1].AddVertex(bbox_max, {}, {});
    out_polys[1].AddVertex({bbox_max.x, bbox_max.y, bbox_min.z}, {}, {});

    out_polys[2].AddVertex(bbox_min, {}, {});
    out_polys[2].AddVertex({bbox_min.x, bbox_min.y, bbox_max.z}, {}, {});
    out_polys[2].AddVertex({bbox_max.x, bbox_min.y, bbox_max.z}, {}, {});
    out_polys[2].AddVertex({bbox_max.x, bbox_min.y, bbox_min.z}, {}, {});    

    out_polys[3].AddVertex({bbox_min.x, bbox_max.y, bbox_min.z}, {}, {});
    out_polys[3].AddVertex({bbox_max.x, bbox_max.y, bbox_min.z}, {}, {});
    out_polys[3].AddVertex(bbox_max, {}, {});
    out_polys[3].AddVertex({bbox_min.x, bbox_max.y, bbox_max.z}, {}, {});
    
    out_polys[4].AddVertex(bbox_min, {}, {});
    out_polys[4].AddVertex({bbox_max.x, bbox_min.y, bbox_min.z}, {}, {});
    out_polys[4].AddVertex({bbox_max.x, bbox_max.y, bbox_min.z}, {}, {});
    out_polys[4].AddVertex({bbox_min.x, bbox_max.y, bbox_min.z}, {}, {});

    out_polys[5].AddVertex({bbox_min.x, bbox_min.y, bbox_max.z}, {}, {});
    out_polys[5].AddVertex({bbox_min.x, bbox_max.y, bbox_max.z}, {}, {});
    out_polys[5].AddVertex(bbox_max, {}, {});
    out_polys[5].AddVertex({bbox_max.x, bbox_min.y, bbox_max.z}, {}, {});

    return out_polys;
}

int bsp::internal::ChooseDividingPoly(const std::vector<Poly> &polys, const std::vector<Plane> &planes,
                                      const std::vector<Poly> &prev_dividers, float eps) {
    const float MIN_RELATION_SCALE = 2;
    float min_relation = 0.8f,
          best_relation = 0;

    int best_poly = -1,
        best_worst_poly = -1;
    
    unsigned least_splits = std::numeric_limits<unsigned>::max();

    bool repeat = false;

    bool convex = true;

    while (best_poly == -1) {
        for (unsigned i = 0; i < polys.size(); i++) {
            if (std::find(prev_dividers.begin(), prev_dividers.end(), polys[i]) != prev_dividers.end()) {
                continue;
            }

            unsigned num_front = 0,
                     num_back = 0,
                     num_spanning = 0;

            for (unsigned j = 0; j < polys.size(); j++) {
                if (i == j) continue;
                switch (planes[i].ClassifyPoly(polys[j], eps)) {
                    case OnPlane:
                        if (glm::dot(planes[i].n, planes[j].n) > 0) {
                            num_front++;
                        } else {
                            num_back++;
                        }
                        break;
                    case Front:
                    case TouchFront:
                        num_front++;
                        break;
                    case Back:
                    case TouchBack:
                        num_back++;
                        break;
                    case Spanning:
                        num_spanning++;
                        break;
                }
            }

            if (num_back || num_spanning) {
                convex = false;
            }
	    
            float relation = 0;
            if (num_front < num_back) {
                if (num_back) {
                    relation = (float)num_front/num_back;
                }
            } else {
                if (num_front) {
                    relation = (float)num_back/num_front;
                }
            }

            if (relation > 0) {
                repeat = true;
            }

            if ((relation > min_relation && num_spanning <= least_splits) ||
                (num_spanning <= least_splits && relation > best_relation)) {
                best_poly = i;
                least_splits = num_spanning;
                best_relation = relation;
            } else if (relation < eps && (num_back || num_spanning)) {
                best_worst_poly = i;
            }
        }

        if (convex) {
            return -1;
        }

        if (!repeat) {
            return best_worst_poly;
        }

        min_relation /= MIN_RELATION_SCALE;
    }

    return best_poly;
}

std::unique_ptr<bsp::Node> bsp::internal::BuildTree(std::vector<Poly> &polys, std::vector<Plane> &planes, float eps) {
    std::vector<Poly> prev_dividers;
    int divider = internal::ChooseDividingPoly(polys, planes, prev_dividers, eps);
    if (divider == -1) {
        return std::unique_ptr<Node>{new Node(polys, planes)};
    }

    prev_dividers.push_back(polys[divider]);

    std::vector<Poly> front_polys = { polys[divider] }, back_polys;
    std::vector<Plane> front_planes = { planes[divider] }, back_planes;

    for (unsigned i = 0; i < polys.size(); i++) {
        if (i == (unsigned)divider) continue;

        switch (planes[divider].ClassifyPoly(polys[i], eps)) {
            case OnPlane:
                if (glm::dot(planes[divider].n, planes[i].n) > 0) {
                    front_polys.push_back(polys[i]);
                    front_planes.push_back(planes[i]);
                } else {
                    back_polys.push_back(polys[i]);
                    back_planes.push_back(planes[i]);
                }
                break;
            case Front:
            case TouchFront:
                front_polys.push_back(polys[i]);
                front_planes.push_back(planes[i]);
                break;
            case Back:
            case TouchBack:
                back_polys.push_back(polys[i]);
                back_planes.push_back(planes[i]);
                break;
            case Spanning:
                auto res = polys[i].SplitByPlane(planes[divider], eps);
                
                front_polys.push_back(std::move(res.first));
                back_polys.push_back(std::move(res.second));

                front_planes.push_back(planes[i]);
                back_planes.push_back(planes[i]);
	    
                break;
        }
    }

    if (front_polys.empty() || back_polys.empty()) {
        throw std::runtime_error("Empty polygon list!");
    }

    std::unique_ptr<Node> front_node = BuildTree(front_polys, front_planes, eps),
                          back_node = BuildTree(back_polys, back_planes, eps);

    return std::unique_ptr<Node>{new Node(planes[divider], std::move(front_node), std::move(back_node))};
}

void bsp::internal::VisualizeTree(bsp::Node *root) {
    using namespace bsp;
    
    std::vector<bool> filled;
    filled.push_back(true);
    auto print_node = [&filled](Node *node) {
        if (node->has_polys()) return;
            
        for (int i = 0; i < (int)filled.size() - 1; i++) {
            printf(filled[i] ? "  " :  "| ");
        }
            
        if (node->parent_node() && node->parent_node()->back_node() == node) {
            filled.back() = true;
        }

        printf("|_%i\n", int(filled.size() - 1));
                
        filled.push_back(false);
        if (!node->front_node() || node->front_node()->has_polys()) {
            for (int i = 0; i < (int)filled.size() - 1; i++) {
                printf(filled[i] ? "  " :  "| ");
            }
            if (node->front_node()) {
                printf("|_[p: %i, pvs: %i]\n",
                       int(node->front_node()->polys().size()),
                       int(node->front_node()->pvs().size()));
            } else {
                printf("|_[null]\n");
            }
        }

        if (!node->back_node() || node->back_node()->has_polys()) {
            for (int i = 0; i < (int)filled.size() - 1; i++) {
                printf(filled[i] ? "  " :  "| ");
            }
            if (node->back_node()) {
                printf("|_[p: %i, pvs: %i]\n",
                       int(node->back_node()->polys().size()),
                       int(node->back_node()->pvs().size()));
            } else {
                printf("|_[null]\n");
            }
            filled.back() = true;
            while (!filled.empty() && filled.back()) filled.pop_back();
        }
    };

    if (root->has_polys()) {
        // single node case
        printf("|_[p: %i, pvs: %i]\n", int(root->polys().size()), int(root->pvs().size()));
        filled.clear();
    } else {
        root->Exec(print_node);
    }
    assert(filled.empty());
}
