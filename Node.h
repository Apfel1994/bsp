#pragma once

#include <memory>
#include <vector>

#include "Plane.h"
#include "Portal.h"

namespace bsp {
    class Brush;

    class Node {
        Plane div_plane_;

        std::unique_ptr<Node> front_node_,
                              back_node_;
        Node *parent_node_;

        std::vector<Poly> polys_;
        std::vector<Plane> planes_;

        std::vector<Portal> portals_;
    
        glm::vec3 bbox_min_, bbox_max_;

        std::vector<Node *> pvs_;
    
        void UpdateBBox();
    public:
        inline Node(const Plane &divider,
                    std::unique_ptr<Node> &&front, std::unique_ptr<Node> &&back);
        inline Node(const std::vector<Poly> &polys, const std::vector<Plane> &planes);
        inline Node(std::vector<Poly> &&polys, std::vector<Plane> &&planes);

        const Plane &div_plane() const { return div_plane_; }

        Node *front_node() { return front_node_.get(); }
        Node *back_node() { return back_node_.get(); }
        Node *parent_node() { return parent_node_; }

        bool has_polys() const { return !polys_.empty(); }

        const std::vector<Poly> &polys() const { return polys_; }
    
        const glm::vec3 &bbox_min() const { return bbox_min_; }
        const glm::vec3 &bbox_max() const { return bbox_max_; }

        const std::vector<Node *> &pvs() const { return pvs_; }

        void set_front_node(std::unique_ptr<Node> &&node) {
            front_node_ = std::move(node);
            UpdateBBox();
            if (front_node_) {
                front_node_->set_parent_node(this);
            }
        }
    
        void set_back_node(std::unique_ptr<Node> &&node) {
            back_node_ = std::move(node);
            UpdateBBox();
            if (back_node_) {
                back_node_->set_parent_node(this);
            }
        }
    
        void set_parent_node(Node *node) { parent_node_ = node; }

        template<typename F>
        void Exec(const F &f) {
            Node *parent = parent_node();
            f(this);

            if (parent &&
                parent->front_node() != this &&
                parent->back_node() != this) return; // in case of deletion

            if (front_node_) {
                front_node_->Exec(f);
            }
            if (back_node_) {
                back_node_->Exec(f);
            }
        }

        template<typename F>
        void ExecForConnected(const F &f) {
            std::vector<Node *> nodes;
            GatherConnected(nodes);

            for (Node *n : nodes) {
                f(n);
            }
        }

        void GatherConnected(std::vector<Node *> &out_nodes) {
            for (Node *n : out_nodes) {
                if (n == this) return;
            }
            out_nodes.push_back(this);
            
            for (auto &p : portals_) {
                if (p.front_node() == this && p.back_node()) {
                    p.back_node()->GatherConnected(out_nodes);
                } else if (p.back_node() == this && p.front_node()) {
                    p.front_node()->GatherConnected(out_nodes);
                }
            }
        }
        
        void AddLeafPortal(const Portal &p, float eps);
        bool QueryPolyFlags(uint32_t flags) const;
        void GeneratePortals(VertexPool *pool, float eps);
        std::vector<Portal> ClipPortal(const Portal &portal, float eps);
        void BuildPVS(float eps);

        void UpdateUVs(float scale);
    
        static void RecursePVS(Node *src_leaf, const Portal *src_portal,
                               Node *trg_leaf, const Portal *trg_portal,
                               float eps, std::vector<Node *> &out_nodes);
    };
}

bsp::Node::Node(const Plane &divider, std::unique_ptr<Node> &&front, std::unique_ptr<Node> &&back)
  : div_plane_(divider), front_node_(std::move(front)),
    back_node_(std::move(back)), parent_node_(nullptr) {
    UpdateBBox();

    front_node_->set_parent_node(this);
    back_node_->set_parent_node(this);
}

bsp::Node::Node(const std::vector<Poly> &polys, const std::vector<Plane> &planes)
  : div_plane_({{0, 0, 0}, 0}),
    front_node_(nullptr), back_node_(nullptr), parent_node_(nullptr), polys_(polys), planes_(planes) {
    UpdateBBox();
}

bsp::Node::Node(std::vector<Poly> &&polys, std::vector<Plane> &&planes)
  : div_plane_({{0, 0, 0}, 0}), front_node_(nullptr), back_node_(nullptr), parent_node_(nullptr),
    polys_(std::move(polys)), planes_(std::move(planes)) {
    UpdateBBox();
}
