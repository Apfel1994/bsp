#pragma once

#include <cstdlib>
#include <vector>

#include <glm/common.hpp>
#include <glm/vec3.hpp>

namespace bsp {
    class VertexPool {
        float eps_;
        std::vector<glm::vec3> positions_;
        std::vector<glm::vec3> normals_;
        std::vector<glm::vec2> uvs_;

        std::vector<unsigned> free_positions_;
        std::vector<unsigned> counters_;
    public:
        VertexPool(float eps, unsigned size) : eps_(eps) {
            positions_.reserve(size);
            normals_.reserve(size);
            uvs_.reserve(size);
            counters_.reserve(size);
        }

        float eps() const { return eps_; }
    
        const glm::vec3 &pos(unsigned index) const { return positions_[index]; }
        const glm::vec3 &normal(unsigned index) const { return normals_[index]; }
        const glm::vec2 &uv(unsigned index) const { return uvs_[index]; }

        unsigned counter(unsigned index) const { return counters_[index]; }

        unsigned AddVertex(const glm::vec3 &pos, const glm::vec3 &normal, const glm::vec2 &uvs) {
            unsigned num_vertices = (unsigned)positions_.size();
            for (unsigned i = 0; i < num_vertices; i++) {
                if (!counters_[i]) continue;
                if (glm::abs(positions_[i][0] - pos[0]) > eps_ ||
                    glm::abs(positions_[i][1] - pos[1]) > eps_ ||
                    glm::abs(positions_[i][2] - pos[2]) > eps_) continue;
                if (glm::abs(normals_[i][0] - normal[0]) > eps_ ||
                    glm::abs(normals_[i][1] - normal[1]) > eps_ ||
                    glm::abs(normals_[i][2] - normal[2]) > eps_) continue;
                if (glm::abs(uvs_[i][0] - uvs[0]) > eps_ ||
                    glm::abs(uvs_[i][1] - uvs[1]) > eps_) continue;
                counters_[i]++;
                return i;
            }
            if (free_positions_.empty()) {
                positions_.push_back(pos);
                normals_.push_back(normal);
                uvs_.push_back(uvs);
                counters_.push_back(1);
                return num_vertices;
            } else {
                unsigned index = free_positions_.back();
                positions_[index] = pos;
                normals_[index] = normal;
                uvs_[index] = uvs;
                counters_[index] = 1;
                free_positions_.pop_back();
                return index;
            }
        }

        void IncreaseCounter(unsigned index) { counters_[index]++; }

        void DecreaseCounter(unsigned index) {
            if (--counters_[index] == 0) {
                if (index == positions_.size() - 1) {
                    positions_.pop_back();
                    normals_.pop_back();
                    uvs_.pop_back();
                    counters_.pop_back();
                } else {
                    free_positions_.push_back(index);
                }
            }
        }

        unsigned Size() const {
            return (unsigned)(positions_.size() - free_positions_.size());
        }
    };

    /*class VertexPool {
        float eps_;
        struct VertexData {
            glm::vec3 pos, normal;
            glm::vec2 uvs;
        };
        union Vertex {
            VertexData data;
            int next_free;
        };
        std::vector<Vertex> vertices_;

        int first_free_;
        unsigned num_free_;
        std::vector<unsigned> counters_;
    public:
        VertexPool(float eps, unsigned size) : eps_(eps), first_free_(-1), num_free_(0) {
            vertices_.reserve(size);
            counters_.reserve(size);
        }

        float eps() const { return eps_; }
    
        const glm::vec3 &pos(unsigned index) const { return vertices_[index].data.pos; }
        const glm::vec3 &normal(unsigned index) const { return vertices_[index].data.normal; }
        const glm::vec2 &uv(unsigned index) const { return vertices_[index].data.uvs; }

        unsigned counter(unsigned index) const { return counters_[index]; }

        unsigned AddVertex(const glm::vec3 &pos, const glm::vec3 &normal, const glm::vec2 &uvs) {
            unsigned num_vertices = (unsigned)vertices_.size();
            for (unsigned i = 0; i < num_vertices; i++) {
                if (!counters_[i]) continue;
                if (glm::abs(vertices_[i].data.pos[0] - pos[0]) > eps_ ||
                    glm::abs(vertices_[i].data.pos[1] - pos[1]) > eps_ ||
                    glm::abs(vertices_[i].data.pos[2] - pos[2]) > eps_) continue;
                if (glm::abs(vertices_[i].data.normal[0] - normal[0]) > eps_ ||
                    glm::abs(vertices_[i].data.normal[1] - normal[1]) > eps_ ||
                    glm::abs(vertices_[i].data.normal[2] - normal[2]) > eps_) continue;
                if (glm::abs(vertices_[i].data.uvs[0] - uvs[0]) > eps_ ||
                    glm::abs(vertices_[i].data.uvs[1] - uvs[1]) > eps_) continue;
                counters_[i]++;
                return i;
            }
            if (first_free_ == -1) {
                vertices_.push_back({pos, normal, uvs});
                counters_.push_back(1);
                return num_vertices;
            } else {
                unsigned index = (unsigned)first_free_;
                first_free_ = vertices_[index].next_free;
                vertices_[index] = {pos, normal, uvs};
                counters_[index] = 1;
                num_free_--;
                return index;
            }
        }

        void IncreaseCounter(unsigned index) { counters_[index]++; }

        void DecreaseCounter(unsigned index) {
            if (--counters_[index] == 0) {
                if (index == vertices_.size() - 1) {
                    vertices_.pop_back();
                    counters_.pop_back();
                } else {
                    vertices_[index].next_free = first_free_;
                    first_free_ = index;
                    num_free_++;
                }
            }
        }

        unsigned Size() const {
            return (unsigned)vertices_.size() - num_free_;
        }
    };*/
}
