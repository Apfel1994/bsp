#pragma once

#include <glm/geometric.hpp>

#include "VertexPool.h"

namespace bsp {
    struct Plane;

    class Poly {
        VertexPool            *pool_;
        std::vector<unsigned>  indices_;
        
        unsigned  mat_id_;
        float     tex_angle_;
        glm::vec4 tex_vec_[2];
        unsigned  smoothing_group_;

        uint32_t flags_;
    public:
        inline Poly(VertexPool *pool) : pool_(pool), mat_id_(0), tex_angle_(0), smoothing_group_(0), flags_(0) {}
        inline Poly(const Poly &rhs);
        inline Poly(Poly &&rhs);
        inline ~Poly();

        inline Poly &operator=(const Poly &rhs);
        inline Poly &operator=(Poly &&rhs);

        inline bool operator==(const Poly &rhs) const;
    
        unsigned num_points() const { return (unsigned)indices_.size(); }

        VertexPool *pool() const { return pool_; }

        unsigned vtx_index(unsigned i) const { return indices_[i]; }
        const glm::vec3 &vtx_pos(unsigned i) const { return pool_->pos(indices_[i]); }
        const glm::vec3 &vtx_normal(unsigned i) const { return pool_->normal(indices_[i]); }
        const glm::vec2 &vtx_uv(unsigned i) const { return pool_->uv(indices_[i]); }

        unsigned mat_id() const { return mat_id_; }
        float tex_angle() const { return tex_angle_; }
        const glm::vec4 &tex_vec(unsigned i) const { return tex_vec_[i]; }
        unsigned smoothing_group() const { return smoothing_group_; }
        uint32_t flags() const { return flags_; }

        inline glm::vec3 center() const;
    
        void set_mat_id(unsigned mat_id) { mat_id_ = mat_id; }
        void set_tex_angle(float angle) { tex_angle_ = angle; }
        void set_tex_vec(const glm::vec4 (&tex_vec)[2]) { tex_vec_[0] = tex_vec[0]; tex_vec_[1] = tex_vec[1]; }
        void set_smoothing_group(unsigned g) { smoothing_group_ = g; }

        void set_flags(uint32_t f) { flags_ |= f; }
        void unset_flags(uint32_t f) { flags_ &= ~f; }
    
        inline void AddVertex(const glm::vec3 &pos, const glm::vec3 &normal, const glm::vec2 &uv);
        inline void AddVertex(unsigned i, VertexPool *pool);
        inline void SetVertex(unsigned i, const glm::vec3 &pos, const glm::vec3 &normal, const glm::vec2 &uv);
    
        enum eWindingOrder { CCW, CW };
        inline void ReorderVertices(const glm::vec3 &normal, eWindingOrder order);

        std::pair<Poly, Poly> SplitByPlane(const Plane &pl, float eps) const;

        static std::vector<Poly> ClipPolyToList(const Plane &pl, const Poly &poly,
                                                std::vector<Plane>::const_iterator clipper_it,
                                                const std::vector<Plane>::const_iterator clippers_end,
                                                bool clip_on_plane, float eps);

        static std::vector<Poly> ClipPolyToListTmp(const Plane &pl, const Poly &poly,
                                                   std::vector<Plane>::const_iterator clipper_it,
                                                   const std::vector<Plane>::const_iterator clippers_end,
                                                   bool clip_on_plane, float eps);

        static bool IsEqual(const Poly &p1, const Poly &p2, float eps);
    };
}

bsp::Poly::Poly(const Poly &rhs) : pool_(rhs.pool_),
        indices_(rhs.indices_),
        mat_id_(rhs.mat_id_),
        tex_angle_(rhs.tex_angle_),
        smoothing_group_(rhs.smoothing_group_),
        flags_(rhs.flags_) {
    for (unsigned i : { 0u, 1u }) {
        tex_vec_[i] = rhs.tex_vec_[i];
    }
    for (unsigned i : indices_) {
        pool_->IncreaseCounter(i);
    }
}

bsp::Poly::Poly(Poly &&rhs) : pool_(rhs.pool_),
        indices_(std::move(rhs.indices_)),
        mat_id_(rhs.mat_id_),
        tex_angle_(rhs.tex_angle_),
        smoothing_group_(rhs.smoothing_group_),
        flags_(rhs.flags_) {
    for (auto i : { 0, 1 }) {
        tex_vec_[i] = rhs.tex_vec_[i];
    }
}

bsp::Poly::~Poly() {
    for (unsigned i : indices_) {
        pool_->DecreaseCounter(i);
    }
}

bsp::Poly &bsp::Poly::operator=(const Poly &rhs) {
    for (unsigned i : indices_) {
        pool_->DecreaseCounter(i);
    }
    pool_            = rhs.pool_;
    indices_         = rhs.indices_;
    mat_id_          = rhs.mat_id_;
    tex_angle_       = rhs.tex_angle_;
    for (unsigned i = 0; i < 2; i++) {
        tex_vec_[i]  = rhs.tex_vec_[i];
    }
    smoothing_group_ = rhs.smoothing_group_;
    flags_           = rhs.flags_;
    for (unsigned i : indices_) {
        pool_->IncreaseCounter(i);
    }
    return *this;
}

bsp::Poly &bsp::Poly::operator=(Poly &&rhs) {
    for (unsigned i : indices_) {
        pool_->DecreaseCounter(i);
    }
    pool_            = rhs.pool_;
    indices_         = std::move(rhs.indices_);
    mat_id_          = rhs.mat_id_;
    tex_angle_       = rhs.tex_angle_;
    for (unsigned i = 0; i < 2; i++) {
        tex_vec_[i]  = rhs.tex_vec_[i];
    }
    smoothing_group_ = rhs.smoothing_group_;
    flags_           = rhs.flags_;
    return *this;
}

inline bool bsp::Poly::operator==(const Poly &rhs) const {
    return IsEqual(*this, rhs, pool_->eps());
}

glm::vec3 bsp::Poly::center() const {
    glm::vec3 ret;
    
    float k = 1.0f / num_points();
    for (unsigned i = 0; i < num_points(); i++) {
        ret += vtx_pos(i) * k;
    }

    return ret;
}

void bsp::Poly::AddVertex(const glm::vec3 &pos, const glm::vec3 &normal, const glm::vec2 &uv) {
    unsigned index = pool_->AddVertex(pos, normal, uv);
    indices_.push_back(index);
}

void bsp::Poly::AddVertex(unsigned i, VertexPool *pool) {
    if (pool == pool_) {
        pool_->IncreaseCounter(i);
        indices_.push_back(i);
    } else {
        AddVertex(pool->pos(i), pool->normal(i), pool->uv(i));
    }
}

void bsp::Poly::SetVertex(unsigned i,
                     const glm::vec3 &pos, const glm::vec3 &normal, const glm::vec2 &uv) {
    unsigned index = pool_->AddVertex(pos, normal, uv);
    pool_->DecreaseCounter(indices_[i]);
    indices_[i] = index;
}


void bsp::Poly::ReorderVertices(const glm::vec3 &normal, eWindingOrder order) {
    using namespace glm;

    assert(order == CCW);

    vec3 center = this->center();

    for (unsigned i = 0; i < num_points() - 1; i++) {
        vec3 a = normalize(vtx_pos(i) - center);
        vec3 v = cross(normal, a); v = normalize(v);

        float d = -glm::dot(v, center);

        float biggest_dot = -std::numeric_limits<float>::max();
        unsigned biggest = 0;

        for (unsigned j = i + 1; j < num_points(); j++) {
            if (glm::dot(v, vtx_pos(j)) + d > 0) {
                vec3 b = normalize(vtx_pos(j) - center);

                float dot1 = glm::dot(a, b);
                if (dot1 > biggest_dot) {
                    biggest_dot = dot1;
                    biggest = j;
                }
            }
        }

        if (biggest != i + 1) {
            std::swap(indices_[i + 1], indices_[biggest]);
        }
    }
}
