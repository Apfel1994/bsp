#include "Portal.h"

std::vector<bsp::Plane> bsp::Portal::ConstructClipPlanes(const Portal &source, const Portal &target, float eps) {
    if (Plane::IsEqual(source.plane(), target.plane(), eps)) {
        return {};
    }

    std::vector<Plane> out_planes;

    const Portal *temp_source, *temp_target;
    
    for (int loop = 0; loop < 2; loop++) {
        if (loop == 0) {
            temp_source = &source;
            temp_target = &target;
        } else {
            temp_source = &target;
            temp_target = &source;
        }

        for (unsigned src_p = 0; src_p < temp_source->poly().num_points(); src_p++) {
            if (temp_target->plane().ClassifyPoint(temp_source->poly().vtx_pos(src_p), eps) == OnPlane) {
                continue;
            }

            for (unsigned trg_p = 0; trg_p < temp_target->poly().num_points(); trg_p++) {
                unsigned next_p = trg_p + 1;
                if (next_p == temp_target->poly().num_points()) {
                    next_p = 0;
                }

                glm::vec3 edge1 = temp_source->poly().vtx_pos(src_p) - temp_target->poly().vtx_pos(trg_p),
                          edge2 = temp_target->poly().vtx_pos(next_p) - temp_target->poly().vtx_pos(trg_p);

                Plane new_plane = {glm::cross(edge1, edge2), 0};
                if (!new_plane.is_valid()) {
                    continue;
                }
                new_plane.d = -glm::dot(new_plane.n, temp_source->poly().vtx_pos(src_p));

                auto src_loc = new_plane.ClassifyPoly(temp_source->poly(), eps);
                auto trg_loc = new_plane.ClassifyPoly(temp_target->poly(), eps);

                /*if (src_loc == Plane::OnPlane) {
                    if (glm::dot(new_plane.n, temp_source->plane().n) > 0) {
                        src_loc = Plane::TouchFront;
                    } else {
                        src_loc = Plane::TouchBack;
                    }
                }

                if (trg_loc == Plane::OnPlane) {
                    if (glm::dot(new_plane.n, temp_target->plane().n) > 0) {
                        trg_loc = Plane::TouchFront;
                    } else {
                        trg_loc = Plane::TouchBack;
                    }
                    }*/

                if (((src_loc == Front || src_loc == TouchFront) &&
                     (trg_loc == Back || trg_loc == TouchBack)) ||
                    ((src_loc == Back || src_loc == TouchBack) &&
                     (trg_loc == Front || trg_loc == TouchFront))) {
                    out_planes.push_back(new_plane);
                }
            }
        }
    }

    return out_planes;
}

bsp::Portal bsp::Portal::ClipToAntiPenumbra(const std::vector<Plane> &clip_planes, const Portal &gen_portal, float eps) const {
    for (auto &clip_plane : clip_planes) {
        auto gen_portal_loc = clip_plane.ClassifyPoly(gen_portal.poly(), eps);
        auto src_portal_loc = clip_plane.ClassifyPoly(poly_, eps);

        if (src_portal_loc == Spanning) {
            src_portal_loc = clip_plane.ClassifyPoint(poly_.center(), eps);
        }

        if (gen_portal_loc == TouchFront) gen_portal_loc = Front;
        if (gen_portal_loc == TouchBack) gen_portal_loc = Back;

        if (src_portal_loc == TouchFront) src_portal_loc = Front;
        if (src_portal_loc == TouchBack) src_portal_loc = Back;

        if (src_portal_loc == OnPlane) {
            if (glm::dot(clip_plane.n, plane_.n) > eps) {
                src_portal_loc = Front;
            } else {
                src_portal_loc = Back;
            }
        }

        if (gen_portal_loc == src_portal_loc || gen_portal_loc == OnPlane) {
            // return empty portal
            return Portal{Poly(gen_portal.poly().pool()), gen_portal.plane(),
                          gen_portal.front_node(), gen_portal.back_node()};
        }
        
        if ((gen_portal_loc == Back && src_portal_loc == Front) ||
            (gen_portal_loc == Front && src_portal_loc == Back)) {
            continue;
        }

        if (gen_portal_loc == Spanning) {
            auto res = gen_portal.poly().SplitByPlane(clip_plane, eps);

            if (src_portal_loc == Front) {
                return {std::move(res.second), gen_portal.plane(),
                        gen_portal.front_node(), gen_portal.back_node()};
            } else if (src_portal_loc == Back) {
                return {std::move(res.first), gen_portal.plane(),
                        gen_portal.front_node(), gen_portal.back_node()};
            }
        }
    }

    return gen_portal;
}
