#undef NDEBUG
#include <cassert>

#include "../Portal.h"

void test_portal() {
    using namespace bsp;
    
    {   // equality
        auto pool1 = VertexPool{0.001f, 100},
             pool2 = VertexPool{0.001f, 100};
        auto pl = Plane{{0, 1, 0}, 0};
        Poly poly1 = {&pool1}, poly2 = {&pool2};
        poly1.AddVertex({0, 0, 0}, {}, {});
        poly1.AddVertex({1, 0, 0}, {}, {});
        poly1.AddVertex({1, 0, 1}, {}, {});

        poly2.AddVertex({0, 0, 0}, {}, {});
        poly2.AddVertex({1, 0, 0}, {}, {});
        poly2.AddVertex({1, 0, 1.0005}, {}, {});

        auto p1 = Portal{poly1, pl, nullptr, nullptr},
             p2 = Portal{poly2, pl, nullptr, nullptr};

        assert(Portal::IsEqual(p1, p2, 0.001f));
    }
    {   // clip planes construction
        auto pool = VertexPool{0.001f, 100};
        auto pl1 = Plane{{0, 1, 0}, 0}, pl2 = Plane{{0, -1, 0}, 1};
        Poly poly1 = {&pool}, poly2 = {&pool};
        poly1.AddVertex({0, 0, 0}, {}, {});
        poly1.AddVertex({1, 0, 0}, {}, {});
        poly1.AddVertex({1, 0, 1}, {}, {});

        poly2.AddVertex({0, 1, 0}, {}, {});
        poly2.AddVertex({0, 1, 1}, {}, {});
        poly2.AddVertex({1, 1, 1}, {}, {});

        auto p1 = Portal{poly1, pl1, nullptr, nullptr},
             p2 = Portal{poly2, pl2, nullptr, nullptr};

        auto planes = Portal::ConstructClipPlanes(p1, p2, pool.eps());
        assert(planes.size() == 12);
    }
}
