
void test_vertex_pool();
void test_plane();
void test_poly();
void test_portal();
void test_brush();
void test_bsp();

int main() {
    test_vertex_pool();
    test_plane();
    test_poly();
    test_portal();
    test_brush();
    test_bsp();
}
