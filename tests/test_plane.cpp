#undef NDEBUG
#include <cassert>

#include "../Plane.h"

void test_plane() {
    using namespace bsp;
    
    {   // normalize normal
        Plane pl = {{0, 2, 0}, 0};
        assert(glm::abs(glm::length(pl.n) - 1) < 0.0001f);
        Plane pl1 = {{0, 0, 0}, 0};
        assert(!pl1.is_valid());
    }

    {   // classify point
        Plane pl = {{0, 1, 0}, 0};
        assert(pl.ClassifyPoint({0, -0.11, 0}, 0.1f) == Back);
        assert(pl.ClassifyPoint({0, 0.11, 0}, 0.1f) == Front);
        assert(pl.ClassifyPoint({0, 0.09, 0}, 0.1f) == OnPlane);
    }

    {   // classify polygon
        Plane pl = {{0, 1, 0}, 0};
        VertexPool pool(0.1f, 100);
        Poly poly1(&pool), poly2(&pool), poly3(&pool);
        poly1.AddVertex({0, 0, 0}, {}, {});
        poly1.AddVertex({1, 0.05f, 0}, {}, {});
        poly1.AddVertex({1, -0.05f, 1}, {}, {});
        assert(pl.ClassifyPoly(poly1, 0.1f) == OnPlane);

        poly2.AddVertex({0, 0, 0}, {}, {});
        poly2.AddVertex({0, 1, 0}, {}, {});
        poly2.AddVertex({1, 2, 0}, {}, {});
        assert(pl.ClassifyPoly(poly2, 0.1f) == TouchFront);

        poly3.AddVertex({0, -0.15f, 0}, {}, {});
        poly3.AddVertex({0, -1, 0}, {}, {});
        poly3.AddVertex({1, -2, 0}, {}, {});
        assert(pl.ClassifyPoly(poly3, 0.1f) == Back);
    }

    {   // test equality
        Plane pl1 = {{1, 0, 0}, 4},
              pl2 = {{1, 0.15f, 0}, 4},
              pl3 = {{1, 0.05f, 0}, 4};
        assert(!Plane::IsEqual(pl1, pl2, 0.1f));
        assert(Plane::IsEqual(pl1, pl3, 0.1f));
    }

    {   // get intersection
        Plane pl1 = {{0, 1, 0}, 0},
              pl2 = {{1, 0, 0}, 0},
              pl3 = {{0, 0, 1}, 0};
        glm::vec3 p;
        assert(GetIntersection(pl1, pl2, pl3, 0.1f, p));
        assert(glm::abs(p.x) < 0.1f && glm::abs(p.y) < 0.1f && glm::abs(p.z) < 0.1f);
    }
}
