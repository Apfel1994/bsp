#undef NDEBUG
#include <cassert>

#include <algorithm>

#include "../BSP.h"

namespace bsp {
    namespace internal {
        std::vector<Plane> BuildBoundingPlanes(const glm::vec3 &bbox_min, const glm::vec3 &bbox_max);
        std::vector<Poly> BuildBoundingPolys(VertexPool *pool, const glm::vec3 &min, const glm::vec3 &max);
    }
}

void test_bsp() {
    using namespace bsp;
    using namespace bsp::internal;
    
    {   // build bounding planes
	    glm::vec3 bbox_min = {0, 0, 0}, bbox_max = {1, 1, 1};
        auto planes = BuildBoundingPlanes(bbox_min, bbox_max);
        assert(planes.size() == 6);

        auto assert_find = [&planes](const Plane &pl1) {
            assert(std::find_if(planes.begin(), planes.end(), [&pl1](const Plane &pl2) {
                        return Plane::IsEqual(pl1, pl2, 0.001f);
                    }) != planes.end());
        };
	
        assert_find({{1, 0, 0}, 0});
        assert_find({{-1, 0, 0}, 1});
        assert_find({{0, 1, 0}, 0});
        assert_find({{0, -1, 0}, 1});
        assert_find({{0, 0, 1}, 0});
        assert_find({{0, 0, -1}, 1});
        
    }
    
    {   // build bounding polys
        glm::vec3 bbox_min = {0, 0, 0}, bbox_max = {1, 1, 1};
        float eps = 0.001f;
        VertexPool pool(eps, 100);
        auto polys = BuildBoundingPolys(&pool, bbox_min, bbox_max);
        assert(polys.size() == 6);

        for (int i = 0; i < 6; i++) {
            assert(polys[i].num_points() == 4);
            for (int j = 0; j < 4; j++) {
                glm::vec3 p = polys[i].vtx_pos(j);
                assert(p.x + eps >= bbox_min.x &&
                       p.y + eps >= bbox_min.y &&
                       p.z + eps >= bbox_min.z);
                assert(p.x - eps <= bbox_max.x &&
                       p.y - eps <= bbox_max.y &&
                       p.z - eps <= bbox_max.z);

            }
        }
    }
}
