#undef NDEBUG
#include <cassert>

#include <glm/geometric.hpp>

#include "../Plane.h"
#include "../Poly.h"

namespace {
    template <typename T>
    bool IsSame(const T &v1, const T &v2, float eps) {
	    return glm::distance(v1, v2) < eps;
    }
}

void test_poly() {
    using namespace bsp;
    using namespace glm;
    
    {   // add points
        VertexPool pool(0.1f, 100);
        Poly poly(&pool);
        poly.AddVertex({0, 0, 0}, {0, 0, 0}, {0, 0});
        poly.AddVertex({1, 0, 0}, {1, 2, 3}, {1, 0});
        poly.AddVertex({1, 0, 1}, {4, 5, 6}, {0, 1});
        assert(poly.num_points() == 3);
        assert(IsSame(poly.vtx_pos(0), vec3{0, 0, 0}, 0.1f));
        assert(IsSame(poly.vtx_normal(0), vec3{ 0, 0, 0 }, 0.1f));
        assert(IsSame(poly.vtx_uv(0), vec2{ 0, 0 }, 0.1f));

        assert(IsSame(poly.vtx_pos(1), vec3{ 1, 0, 0 }, 0.1f));
        assert(IsSame(poly.vtx_normal(1), vec3{ 1, 2, 3 }, 0.1f));
        assert(IsSame(poly.vtx_uv(1), vec2{ 1, 0 }, 0.1f));

        assert(IsSame(poly.vtx_pos(2), vec3{ 1, 0, 1 }, 0.1f));
        assert(IsSame(poly.vtx_normal(2), vec3{ 4, 5, 6 }, 0.1f));
        assert(IsSame(poly.vtx_uv(2), vec2{ 0, 1 }, 0.1f));
    }

    {   // copy - assign - compare
        VertexPool pool(0.1f, 100);
        Poly poly(&pool);
        poly.AddVertex({0, 0, 0}, {0, 0, 0}, {0, 0});
        poly.AddVertex({1, 0, 0}, {1, 2, 3}, {1, 0});
        poly.AddVertex({1, 0, 1}, {4, 5, 6}, {0, 1});
        poly.set_mat_id(13);
        poly.set_tex_angle(35.0f);
        poly.set_tex_vec({{0, 0, 0, 0}, {1, 2, 3, 4}});
        poly.set_smoothing_group(11);
        {
            Poly poly2(poly);
	    
            assert(poly2.num_points() == 3);
            assert(IsSame(poly2.vtx_pos(0), poly.vtx_pos(0), 0.1f));
            assert(IsSame(poly2.vtx_normal(0), poly.vtx_normal(0), 0.1f));
            assert(IsSame(poly2.vtx_uv(0), poly.vtx_uv(0), 0.1f));

            assert(IsSame(poly2.vtx_pos(1), poly.vtx_pos(1), 0.1f));
            assert(IsSame(poly2.vtx_normal(1), poly.vtx_normal(1), 0.1f));
            assert(IsSame(poly2.vtx_uv(1), poly.vtx_uv(1), 0.1f));

            assert(IsSame(poly2.vtx_pos(2), poly.vtx_pos(2), 0.1f));
            assert(IsSame(poly2.vtx_normal(2), poly.vtx_normal(2), 0.1f));
            assert(IsSame(poly2.vtx_uv(2), poly.vtx_uv(2), 0.1f));

            assert(poly2.mat_id() == poly.mat_id());
            assert(IsSame(poly.tex_angle(), poly.tex_angle(), 0.01f));
            assert(IsSame(poly2.tex_vec(0), poly.tex_vec(0), 0.01f));
            assert(IsSame(poly2.tex_vec(1), poly.tex_vec(1), 0.01f));
            assert(poly2.smoothing_group() == poly.smoothing_group());
	    
            assert(poly2 == poly);

            assert(pool.counter(0) == 2);
            assert(pool.counter(1) == 2);
            assert(pool.counter(2) == 2);
        }

        {
            VertexPool pool2(0.1f, 100);
            Poly poly1(&pool2);
            poly1.AddVertex({0, 0, 0}, {0, 0, 0}, {0, 0});
            poly1.AddVertex({1, 0, 0}, {1, 2, 3}, {1, 0});
            poly1.AddVertex({1, 0, 1}, {4, 5, 6}, {0, 1});
            poly1 = poly;

            assert(pool2.Size() == 0);

            Poly poly2(&pool2);
            poly2.AddVertex({0, 0, 0}, {0, 0, 0}, {0, 0});
            poly2.AddVertex({1, 0, 0}, {1, 2, 3}, {1, 0});
            poly2.AddVertex({1, 0, 1}, {4, 5, 6}, {0, 1});

            Poly poly3(&pool);
            poly3.AddVertex({0, 0, 0}, {0, 0, 0}, {0, 0});
            poly3.AddVertex({1, 0, 0}, {1, 2, 3}, {1, 0});
            poly3.AddVertex({1, 0, 1}, {4, 5, 6}, {0, 1});
            
            poly2 = std::move(poly3);
            assert(pool2.Size() == 0);
        }

        assert(pool.counter(0) == 1);
        assert(pool.counter(1) == 1);
        assert(pool.counter(2) == 1);
    }

    {   // set winding order
        VertexPool pool(0.1f, 100);
        Poly poly(&pool);
        poly.AddVertex({0, 0, 0}, {}, {});
        poly.AddVertex({1, 0, 0}, {}, {});
        poly.AddVertex({1, 0, 1}, {}, {});

        poly.ReorderVertices({0, 1, 0}, Poly::CCW);
        assert(IsSame(poly.vtx_pos(0), vec3{ 0, 0, 0 }, 0.1f));
        assert(IsSame(poly.vtx_pos(1), vec3{ 1, 0, 1 }, 0.1f));
        assert(IsSame(poly.vtx_pos(2), vec3{ 1, 0, 0 }, 0.1f));

        //poly.ReorderVertices({0, 1, 0}, Poly::CW);
    }

    {   // split
        VertexPool pool(0.1f, 100);
        Poly poly(&pool);
        poly.AddVertex({-1, 0, -1}, {}, {});
        poly.AddVertex({-1, 0, 1}, {}, {});
        poly.AddVertex({1, 0, 1}, {}, {});
        poly.AddVertex({1, 0, -1}, {}, {});

        Plane pl = {{-1, 0, 0}, 0};
        auto res = poly.SplitByPlane(pl, 0.01f);
        Poly p1 = std::move(res.first), p2 = std::move(res.second);

        assert(p1.num_points() == 4);
        assert(p2.num_points() == 4);
        assert(p1.mat_id() == poly.mat_id());
        assert(IsSame(p1.tex_angle(), poly.tex_angle(), 0.01f));
        assert(IsSame(p1.tex_vec(0), poly.tex_vec(0), 0.01f));
        assert(IsSame(p1.tex_vec(1), poly.tex_vec(1), 0.01f));

        assert(IsSame(p1.vtx_pos(0), vec3{ -1, 0, -1 }, 0.01f));
        assert(IsSame(p1.vtx_pos(1), vec3{ -1, 0, 1 }, 0.01f));
        assert(IsSame(p1.vtx_pos(2), vec3{ 0, 0, 1 }, 0.01f));
        assert(IsSame(p1.vtx_pos(3), vec3{ 0, 0, -1 }, 0.01f));

        assert(IsSame(p2.vtx_pos(0), vec3{ 0, 0, 1 }, 0.01f));
        assert(IsSame(p2.vtx_pos(1), vec3{ 1, 0, 1 }, 0.01f));
        assert(IsSame(p2.vtx_pos(2), vec3{ 1, 0, -1 }, 0.01f));
        assert(IsSame(p2.vtx_pos(3), vec3{ 0, 0, -1 }, 0.01f));
    }
}
