#undef NDEBUG
#include <cassert>

#include <glm/geometric.hpp>

#include "../VertexPool.h"

void test_vertex_pool() {
    using namespace bsp;
    using namespace glm;
    
    {   // add vertices
        VertexPool pool(0.01f, 100);
        assert(pool.AddVertex({0, 0, 0}, {0, 1, 0}, {0, 0}) == 0);
        assert(pool.AddVertex({0, 0, 0}, {0, 1, 0}, {0, 0}) == 0);
        assert(pool.AddVertex({0, 0.011f, 0}, {0, 1, 0}, {0, 0}) == 1);
        assert(pool.AddVertex({0, 0.011f, 0}, {0.011f, 1, 0}, {0, 0}) == 2);
        assert(pool.AddVertex({0, 0.011f, 0}, {0, 1, 0}, {-0.011f, 0}) == 3);
        assert(pool.AddVertex({0.005f, 0, 0}, {0, 1, 0}, {0, 0}) == 0);
        assert(pool.Size() == 4);
    }

    {   // get vertices
        const float eps = 0.01f;
        VertexPool pool(eps, 100);
        unsigned i1 = pool.AddVertex({0, 0, 0}, {0, 1, 0}, {0, 0});
        unsigned i2 = pool.AddVertex({0, 0, 0}, {0, 1, 0}, {0, 0});
        pool.AddVertex({0, 0.011f, 0}, {0, 1, 0}, {0, 0});
        pool.AddVertex({0, 0.011f, 0}, {0.011f, 1, 0}, {0, 0});
        pool.AddVertex({0, 0.011f, 0}, {0, 1, 0}, {-0.011f, 0});
        unsigned i6 = pool.AddVertex({0.005f, 0, 0}, {0, 1, 0}, {0, 0});

        for (auto i : { i1, i2, i6 }) {
            assert(glm::distance(pool.pos(i), vec3{ 0, 0, 0 }) < eps);
            assert(glm::distance(pool.normal(i), vec3{ 0, 1, 0 }) < eps);
            assert(glm::distance(pool.uv(i), vec2{ 0, 0 }) < eps);
        }
    }

    {   // erase vertices
        VertexPool pool(0.01f, 100);
        unsigned i1 = pool.AddVertex({0, 0, 0}, {0, 1, 0}, {0, 0});
        unsigned i2 = pool.AddVertex({0, 0, 0}, {0, 1, 0}, {0.5f, 0});
        unsigned i3 = pool.AddVertex({0, 0, 0}, {0, 1, 0}, {0, 0});
        assert(pool.Size() == 2);
        pool.DecreaseCounter(i1);
        assert(pool.Size() == 2);
        pool.DecreaseCounter(i3);
        assert(pool.Size() == 1);
        pool.DecreaseCounter(i2);
        assert(pool.Size() == 0);
    }
}
