#undef NDEBUG
#include <cassert>

#include <algorithm>

#include "../Brush.h"

namespace {
    template <typename T>
    bool IsSame(const T &v1, const T &v2, float eps) {
        return glm::distance(v1, v2) < eps;
    }
}

void test_brush() {
    using namespace bsp;
    using namespace glm;
    
    {   // create from sides
        const float eps = 0.001f;
        VertexPool pool(eps, 100);

        std::vector<Brush::Side> sides = {{{0, 0, 0},    {1, 0, 0},    {1, 0, 1}},
                                          {{0, 0, 0},    {0, 1, 0},    {0, 0, 1}},
                                          {{0, 0, 0},    {0, 1, 0},    {1, 0, 0}},
                                          {{1, 1, 1},    {0, 1, 1},    {0, 1, 0}},
                                          {{1, 1, 1},    {1, 0, 1},    {0, 0, 1}},
                                          {{1, 1, 1},    {1, 0, 1},    {1, 0, 0}}};
        Brush br(&pool, sides);

        assert(br.num_sides() == 6);
        assert(IsSame(br.bbox_min(), vec3{ 0, 0, 0 }, eps));
        assert(IsSame(br.bbox_max(), vec3{ 1, 1, 1 }, eps));

        glm::vec3 possible_points[] = {{0, 0, 0}, {1, 0, 0}, {0, 0, 1}, {1, 0, 1},
                                       {0, 1, 0}, {1, 1, 0}, {0, 1, 1}, {1, 1, 1}};
        
        br.InitPolys(eps);
        for (int i = 0; i < 6; i++) {
            const auto &poly = br.poly(i);
            assert(poly.num_points() == 4);
            for (int j = 0; j < 4; j++) {
                const glm::vec3 &point = poly.vtx_pos(j);
                assert(std::find_if(std::begin(possible_points), std::end(possible_points),
                                    [=](const glm::vec3 &p) {
                                        return IsSame(point, p, eps);
                                    }) != std::end(possible_points));
            }
        }
    }
}
