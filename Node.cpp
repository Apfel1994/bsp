#include "Node.h"

namespace bsp {
    namespace internal {
        std::vector<Plane> BuildBoundingPlanes(const glm::vec3 &bbox_min, const glm::vec3 &bbox_max);
    }
}

void bsp::Node::UpdateBBox() {
    if (has_polys()) {
        bbox_min_ = glm::vec3(std::numeric_limits<float>::max());
        bbox_max_ = glm::vec3(-std::numeric_limits<float>::max());

        for (auto &p : polys_) {
            for (unsigned i = 0; i < p.num_points(); i++) {
                bbox_min_ = glm::min(bbox_min_, p.vtx_pos(i));
                bbox_max_ = glm::max(bbox_max_, p.vtx_pos(i));
            }
        }
    } else {
        if (!front_node_ && !back_node_) {
            bbox_min_ = bbox_max_ = {};
        } else {
            bbox_min_ = glm::vec3(std::numeric_limits<float>::max());
            bbox_max_ = glm::vec3(-std::numeric_limits<float>::max());

            for (Node *node : {front_node_.get(), back_node_.get()}) {
                if (node) {
                    bbox_min_ = glm::min(bbox_min_, node->bbox_min_);
                    bbox_max_ = glm::max(bbox_max_, node->bbox_max_);            
                }
            }
        }
    }
}

void bsp::Node::AddLeafPortal(const Portal &p, float eps) {
    assert(has_polys());
    if (std::find_if(portals_.begin(), portals_.end(), [p, eps](const Portal &portal) {
                return Portal::IsEqual(p, portal, eps);
            }) == portals_.end()) {
        portals_.push_back(p);
    }
}

bool bsp::Node::QueryPolyFlags(uint32_t flags) const {
    for (auto &p : polys_) {
        if (p.flags() & flags) {
            return true;
        }
    }
    return false;
}

void bsp::Node::GeneratePortals(VertexPool *pool, float eps) {
    if (has_polys()) return;

    glm::vec3 up;
    if (glm::abs(div_plane_.n[0]) <= glm::abs(div_plane_.n[1]) &&
        glm::abs(div_plane_.n[0]) <= glm::abs(div_plane_.n[2])) {
        up[0] = 1;
    } else if (glm::abs(div_plane_.n[1]) <= glm::abs(div_plane_.n[0]) &&
               glm::abs(div_plane_.n[1]) <= glm::abs(div_plane_.n[2])) {
        up[1] = 1;
    } else if (glm::abs(div_plane_.n[2]) <= glm::abs(div_plane_.n[0]) &&
               glm::abs(div_plane_.n[2]) <= glm::abs(div_plane_.n[1])) {
        up[2] = 1;
    }

    glm::vec3 center = -div_plane_.n * div_plane_.d;
    glm::vec3 side = glm::cross(div_plane_.n, up);
    up = glm::cross(div_plane_.n, side);
    up = glm::normalize(up);

    glm::vec3 v = { (bbox_max_ + bbox_min_) / 2.0f - center };
    center += glm::dot(side, v) * side;
    center += glm::dot(up, v) * up;
    
    for (int i = 0; i < 3; i++) {
        assert(center[i] + eps > bbox_min_[i] && center[i] < bbox_max_[i] + eps);
    }
    
    float max_dist = glm::distance(bbox_min_, bbox_max_);
    const glm::vec3 points[4] = {center + side * max_dist + up * max_dist,
                                 center - side * max_dist + up * max_dist,
                                 center - side * max_dist - up * max_dist,
                                 center + side * max_dist - up * max_dist};

    Poly initial_poly(pool);
    for (int i = 0; i < 4; i++) {
        initial_poly.AddVertex(points[i], {}, {});
    }
    initial_poly.ReorderVertices(div_plane_.n, Poly::CCW);
    
    {   // clip to node's bounding box
        auto planes = internal::BuildBoundingPlanes(bbox_min_, bbox_max_);
        auto res = Poly::ClipPolyToList(div_plane_, initial_poly,
                                        planes.begin(), planes.end(), false, eps);
        assert(res.size() == 1);
        initial_poly = std::move(res[0]);
    }
    
    Portal initial_portal(initial_poly, div_plane_, nullptr, nullptr);
    
    portals_ = ClipPortal(initial_portal, eps);

    for (auto it = portals_.begin(); it != portals_.end(); ) {
        if (!it->front_node() || !it->back_node()) {
            it = portals_.erase(it);
        } else {
            it->front_node()->AddLeafPortal(*it, eps);
            it->back_node()->AddLeafPortal(*it, eps);
            ++it;
        }
    }
}

std::vector<bsp::Portal> bsp::Node::ClipPortal(const Portal &portal, float eps) {
    if (has_polys()) {
        std::vector<Portal> ret_portals;
        std::vector<Poly>
            polys_to_add = Poly::ClipPolyToListTmp(portal.plane(), portal.poly(),
                                                   planes_.begin(), planes_.end(), false, eps);

        for (unsigned j = 0; j < polys_to_add.size(); j++) {
            ret_portals.emplace_back(polys_to_add[j], portal.plane(),
                                     portal.front_node(), portal.back_node());
        }

        for (auto &p : ret_portals) {
            for (unsigned i = 0; i < polys_.size(); i++) {
                int loc = p.plane().ClassifyPoly(polys_[i], eps);
                if (loc == OnPlane) {
                    if (glm::dot(p.plane().n, planes_[i].n) > 0) {
                        p.set_front_node(this);
                    } else {
                        p.set_back_node(this);
                    }
                } else if (loc == Front || loc == TouchFront) {
                    p.set_front_node(this);
                    break;
                } else if (loc == Back || loc == TouchBack) {
                    p.set_back_node(this);
                    break;
                }
            }
        }

        return ret_portals;
    } else {
        switch (div_plane_.ClassifyPoly(portal.poly(), eps)) {
            case OnPlane: {
                std::vector<Portal> final_list;
                auto front_list = front_node_->ClipPortal(portal, eps);
                for (auto &p : front_list) {
                    auto back_list = back_node_->ClipPortal(p, eps);
                    final_list.insert(final_list.end(), std::make_move_iterator(back_list.begin()),
                                                        std::make_move_iterator(back_list.end()));
                }
                return final_list;
            }
            case Spanning: {
                auto res = portal.poly().SplitByPlane(div_plane_, eps);
                Portal front(std::move(res.first), div_plane_, portal.front_node(), portal.back_node()),
                       back(std::move(res.second), div_plane_, portal.front_node(), portal.back_node());
                auto front_list = front_node_->ClipPortal(front, eps);
                auto back_list = back_node_->ClipPortal(back, eps);
                front_list.insert(front_list.end(), std::make_move_iterator(back_list.begin()),
                                                    std::make_move_iterator(back_list.end()));
                return front_list;
            }
            case Front:
            case TouchFront:
                return front_node_->ClipPortal(portal, eps);
            case Back:
            case TouchBack:
                return back_node_->ClipPortal(portal, eps);
        }
    }

    return {};
}

void bsp::Node::BuildPVS(float eps) {
    if (polys_.empty()) return;
    for (auto &src_portal : portals_) {
        Node *trg_leaf;
        if (src_portal.front_node() == this) {
            trg_leaf = src_portal.back_node();
        } else {
            trg_leaf = src_portal.front_node();
        }

        pvs_.push_back(trg_leaf);

        for (auto &trg_portal : trg_leaf->portals_) {
            if (!Plane::IsEqual(src_portal.plane(), trg_portal.plane(), eps) &&
                !Plane::IsEqual(src_portal.plane(), Plane(-trg_portal.plane().n, -trg_portal.plane().d), eps)) {
                RecursePVS(this, &src_portal, trg_leaf, &trg_portal, eps, pvs_);
            }
        }
    }

    pvs_.erase(std::unique(pvs_.begin(), pvs_.end()), pvs_.end());
}

void bsp::Node::RecursePVS(Node *src_leaf, const Portal *src_portal,
                           Node *trg_leaf, const Portal *trg_portal,
                           float eps, std::vector<Node *> &out_nodes) {
    Node *gen_leaf;
    if (trg_leaf == trg_portal->front_node()) {
        gen_leaf = trg_portal->back_node();
    } else {
        gen_leaf = trg_portal->front_node();
    }

    out_nodes.push_back(gen_leaf);

    auto clip_planes = Portal::ConstructClipPlanes(*src_portal, *trg_portal, eps);
    if (clip_planes.empty()) return;

    for (const auto &gen_portal : gen_leaf->portals_) {
        if (&gen_portal == trg_portal) continue; // ????
        
        auto gen_portal_loc = src_portal->plane().ClassifyPoly(gen_portal.poly(), eps);
        if (gen_portal_loc == TouchFront) gen_portal_loc = Front;
        if (gen_portal_loc == TouchBack) gen_portal_loc = Back;

        auto src_leaf_loc = Front;
        if (src_portal->back_node() == src_leaf) src_leaf_loc = Back;

        if (gen_portal_loc != OnPlane && gen_portal_loc != src_leaf_loc) {
            gen_portal_loc = trg_portal->plane().ClassifyPoly(gen_portal.poly(), eps);
            if (gen_portal_loc == TouchFront) gen_portal_loc = Front;
            if (gen_portal_loc == TouchBack) gen_portal_loc = Back;

            auto trg_leaf_loc = Front;
            if (trg_portal->back_node() == trg_leaf) trg_leaf_loc = Back;

            if (gen_portal_loc != OnPlane && gen_portal_loc != trg_leaf_loc) {
                Portal clipped_gen_portal = src_portal->ClipToAntiPenumbra(clip_planes, gen_portal, eps);
                if (!clipped_gen_portal.poly().num_points()) {
                    continue;
                }

                auto clip_planes2 = Portal::ConstructClipPlanes(clipped_gen_portal, *trg_portal, eps);
                if (clip_planes2.empty()) continue;
                Portal clipped_src_portal = clipped_gen_portal.ClipToAntiPenumbra(clip_planes2, *src_portal, eps);
                if (!clipped_src_portal.poly().num_points()) {
                    continue;
                }

                RecursePVS(src_leaf, &clipped_src_portal, gen_leaf, &clipped_gen_portal, eps, out_nodes);
            }
        }
    }
}

void bsp::Node::UpdateUVs(float scale) {
    for (unsigned i = 0; i < polys_.size(); i++) {
        Brush::UpdatePolyUVs(polys_[i], planes_[i].n, scale);
    }
}
