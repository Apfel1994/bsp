#pragma once

#include <memory>

#include "Brush.h"
#include "Node.h"

namespace bsp {
    std::unique_ptr<Node> BuildFromBrushes(const std::vector<Brush> &clipped_brushes, VertexPool *pool);
}

