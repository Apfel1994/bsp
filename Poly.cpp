#include "Poly.h"

#include <glm/gtx/norm.hpp>

#include "Plane.h"

namespace bsp {
    namespace PolyConstants {
        const int MAX_POLY_POINTS = 256;
    }
}

std::pair<bsp::Poly, bsp::Poly> bsp::Poly::SplitByPlane(const Plane &pl, float eps) const {
    Poly front = {pool_}, back = {pool_};
    
    unsigned num_points = indices_.size();
    std::vector<int> locations(num_points);
    //int locations[PolyConstants::MAX_POLY_POINTS];

    front.mat_id_ = back.mat_id_ = mat_id_;
    front.tex_angle_ = back.tex_angle_ = tex_angle_;
    for (unsigned i = 0; i < 2; i++) {
        front.tex_vec_[i] = back.tex_vec_[i] = tex_vec_[i];
    }    
    front.smoothing_group_ = back.smoothing_group_ = smoothing_group_;
    front.flags_ = back.flags_ = flags_;

    for (unsigned i = 0; i < num_points; i++) {
        float res = glm::dot(vtx_pos(i), pl.n) + pl.d;
        if (res > eps) {
            locations[i] = Front;
        } else if (res < -eps) {
            locations[i] = Back;
        } else {
            locations[i] = OnPlane;
        }
    }

    for (unsigned i = 0; i < num_points; i++) {
        unsigned j = i + 1;
        bool ignore = false;

        if (i == num_points - 1) {
            j = 0;
        }

        switch (locations[i]) {
            case Front:
                front.AddVertex(indices_[i], pool_);
                break;
            case Back:
                back.AddVertex(indices_[i], pool_);
                break;
            case OnPlane:
                front.AddVertex(indices_[i], pool_);
                back.AddVertex(indices_[i], pool_);
                break;
        }

        if ((locations[i] == OnPlane && locations[j] != OnPlane) ||
            (locations[i] != OnPlane && locations[j] == OnPlane)) {
            ignore = true;
        }

        if (!ignore && locations[i] != locations[j]) {
            glm::vec3 edge_dir = {vtx_pos(i) - vtx_pos(j)};
            edge_dir = glm::normalize(edge_dir);

            float denom = glm::dot(pl.n, edge_dir);

            if (denom < eps && denom > -eps) {
                continue;
            }
            float t = -(glm::dot(pl.n, vtx_pos(i)) + pl.d) / denom;

            glm::vec3 new_pos = {vtx_pos(i) + t * edge_dir};
            front.AddVertex(new_pos, {}, {});
            back.AddVertex(new_pos, {}, {});
        }
    }

    return { front, back };
}

std::vector<bsp::Poly> bsp::Poly::ClipPolyToList(const Plane &pl, const Poly &poly,
                                                 std::vector<Plane>::const_iterator clipper_it,
                                                 const std::vector<Plane>::const_iterator clippers_end,
                                                 bool clip_on_plane, float eps) {
    if (clipper_it == clippers_end) {
        return {};
    }
    
    switch(clipper_it->ClassifyPoly(poly, eps)) {
        case TouchFront:
        case Front:
            return { poly };
        case TouchBack:
        case Back:
            return ClipPolyToList(pl, poly, ++clipper_it, clippers_end, clip_on_plane, eps);
        case OnPlane:
            if (!clip_on_plane && glm::dot(pl.n, clipper_it->n) > 0) {
                return { poly };
            } else {
                return ClipPolyToList(pl, poly, ++clipper_it, clippers_end, clip_on_plane, eps);
            }
        case Spanning:
            auto res = poly.SplitByPlane(*clipper_it, eps);
            Poly front = std::move(res.first), back = std::move(res.second);
	
            std::vector<Poly> back_frags
                = ClipPolyToList(pl, back, ++clipper_it, clippers_end, clip_on_plane, eps);

            if (back_frags.empty()) {
                return { front };
            } else if (back_frags.size() == 1 && back_frags[0] == back) {
                return { poly };
            }

            std::vector<Poly> ret = { std::move(front) };
            ret.insert(ret.end(),
                       std::make_move_iterator(back_frags.begin()),
                       std::make_move_iterator(back_frags.end()));
            return ret;
    }

    return {};
}

std::vector<bsp::Poly> bsp::Poly::ClipPolyToListTmp(const Plane &pl, const Poly &poly,
                                                    std::vector<Plane>::const_iterator clipper_it,
                                                    const std::vector<Plane>::const_iterator clippers_end,
                                                    bool clip_on_plane, float eps) {
    if (clipper_it == clippers_end) {
        return { poly };
    }
    
    switch (clipper_it->ClassifyPoly(poly, eps)) {
        case OnPlane:
        case TouchFront:
        case Front:
            return ClipPolyToListTmp(pl, poly, ++clipper_it, clippers_end, clip_on_plane, eps);
        case TouchBack:
        case Back:
            return {};
        case Spanning:
            auto res = poly.SplitByPlane(*clipper_it, eps);
            Poly front = std::move(res.first), back = std::move(res.second);

            return ClipPolyToListTmp(pl, front, ++clipper_it, clippers_end, clip_on_plane, eps);
    }
    
    return {};
}


bool bsp::Poly::IsEqual(const Poly &p1, const Poly &p2, float eps) {
    if (p1.indices_.size() != p2.indices_.size() ||
        p1.mat_id_ != p2.mat_id_ ||
        p1.tex_angle_ != p2.tex_angle_ ||
        p1.tex_vec_[0] != p2.tex_vec_[0] ||
        p1.tex_vec_[1] != p2.tex_vec_[1] ||
        p1.smoothing_group_ != p2.smoothing_group_ ||
        p1.flags_ != p2.flags_) {
        return false;
    }

    if (p1.pool_ == p2.pool_) {
        return p1.indices_ == p2.indices_;
    } else {
        eps *= eps;
        for (unsigned i = 0; i < p1.num_points(); i++) {
            if (glm::distance2(p1.vtx_pos(i), p2.vtx_pos(i)) > eps ||
                glm::distance2(p1.vtx_normal(i), p2.vtx_normal(i)) > eps ||
                glm::distance2(p1.vtx_uv(i), p2.vtx_uv(i)) > eps) {
                return false;
            }
        }
        return true;
    }
}

