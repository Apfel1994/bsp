#include "Brush.h"

#include <glm/ext.hpp>
#include <glm/gtc/type_ptr.hpp>

bsp::Brush::Brush(VertexPool *pool, const std::vector<Side> &sides) : pool_(pool), flags_(0) {
    planes_.resize(sides.size(), {{0, 0, 0}, 0});

    glm::vec3 center;

    for (unsigned int i = 0; i < sides.size(); i++) {
        const glm::vec3 &p1 = sides[i].p1,
                        &p2 = sides[i].p2,
                        &p3 = sides[i].p3;

        glm::vec3 v1 = p2 - p1,
                  v2 = p3 - p1;

        planes_[i].n = glm::normalize(glm::cross(v1, v2));
        planes_[i].d = -glm::dot(planes_[i].n, p1);

        bbox_min_ = glm::min(bbox_min_, glm::min(p1, glm::min(p2, p3)));
        bbox_max_ = glm::max(bbox_max_, glm::max(p1, glm::max(p2, p3)));

        center += p1 + p2 + p3;
    }

    center /= planes_.size() * 3;

    for (auto &plane : planes_) {
        if (glm::dot(center, plane.n) + plane.d > 0) {
            plane.n *= -1;
            plane.d *= -1;
        }
    }

}

void bsp::Brush::InitPolys(float eps) {
    const float eps_sqr = eps * eps;

    polys_.resize(planes_.size(), pool_);

    for (unsigned i = 0; i < planes_.size() - 2; i++) {
        for (unsigned j = 0; j < planes_.size() - 1; j++) {
            for (unsigned k = 0; k < planes_.size(); k++) {
                if ((i < j) && (j < k)) {
                    glm::vec3 new_vtx_pos;
                    bool legal = GetIntersection(planes_[i], planes_[j], planes_[k], eps, new_vtx_pos);

                    if (legal) {
                        for (unsigned m = 0; m < planes_.size(); m++) {
                            if ((m != i) && (m != j) && (m != k) &&
                                planes_[m].ClassifyPoint(new_vtx_pos, eps) == Front) {
                                legal = false;
                                break;
                            }
                        }
                    }

                    if (legal) {
                        for (auto poly_index : {i, j, k}) {
                            bool unique = true;

                            unsigned num_points = polys_[poly_index].num_points();
                            for (unsigned m = 0; m < num_points; m++) {
                                if (glm::distance2(polys_[poly_index].vtx_pos(m), new_vtx_pos) < eps_sqr) {
                                    unique = false;
                                    break;
                                }
                            }

                            if (unique) {
                                polys_[poly_index].AddVertex(new_vtx_pos, {}, {});
                            }
                        }
                    }
                }
            }
        }
    }

    for (unsigned i = 0; i < polys_.size(); ) {
        if (polys_[i].num_points() < 3) {
            polys_.erase(polys_.begin() + i);
            planes_.erase(planes_.begin() + i);
        } else {
            polys_[i].ReorderVertices(planes_[i].n, Poly::CCW);

            glm::vec3 n = planes_[i].n;
            if (n.x > n.y && n.x > n.z) {
                polys_[i].set_smoothing_group(n.x > 0 ? 0 : 3);
            } else if (n.y > n.x && n.y > n.z) {
                polys_[i].set_smoothing_group(n.y > 0 ? 1 : 4);
            } else {
                polys_[i].set_smoothing_group(n.z > 0 ? 2 : 5);
            }
	    
            i++;
        }
    }
}

void bsp::Brush::UpdateUVs(float scale) {
    for (unsigned i = 0; i < polys_.size(); i++) {
        UpdatePolyUVs(polys_[i], planes_[i].n, scale);
    }
}

void bsp::Brush::SetTexProperties(unsigned i, unsigned mat_id, const glm::vec4 (&tex_vec)[2], float angle) {
    polys_[i].set_mat_id(mat_id);
    polys_[i].set_tex_angle(angle);
    polys_[i].set_tex_vec(tex_vec);
}

void bsp::Brush::SetSmoothingGroup(unsigned i, unsigned val) {
    polys_[i].set_smoothing_group(val);
}

void bsp::Brush::ClipToBrush(const Brush &brush, bool clip_on_plane, float eps) {
    std::vector<Poly> new_polys;
    std::vector<Plane> new_planes;

    for (unsigned i = 0; i < planes_.size(); i++) {
        std::vector<Poly>
            polys_to_add = Poly::ClipPolyToList(planes_[i], polys_[i],
                                                brush.planes_.begin(), brush.planes_.end(),
                                                clip_on_plane, eps);
	
        new_polys.insert(new_polys.end(),
                         std::make_move_iterator(polys_to_add.begin()),
                         std::make_move_iterator(polys_to_add.end()));

        for (unsigned j = 0; j < polys_to_add.size(); j++) {
            new_planes.push_back(planes_[i]);
        }
    }

    polys_ = std::move(new_polys);
    planes_ = std::move(new_planes);
}

void bsp::Brush::UpdatePolyUVs(Poly &poly, const glm::vec3 &n, float tex_scale) {
    const float eps = 0.001f;

    float sum = glm::abs(poly.tex_vec(0).x) +
                glm::abs(poly.tex_vec(0).y) +
                glm::abs(poly.tex_vec(0).z);

    float mag_u = 1, mag_v = 1;
    if (sum > eps) {
        mag_u = glm::sqrt(glm::dot(poly.tex_vec(0), poly.tex_vec(0)));
        mag_v = glm::sqrt(glm::dot(poly.tex_vec(1), poly.tex_vec(1)));
    }

    if (n.x > n.y && n.x > n.z) {
        poly.set_tex_vec({{0, 0, 1, poly.tex_vec(0).w}, {0, 1, 0, poly.tex_vec(1).w}});
    } else if (n.y > n.x && n.y > n.z) {
        poly.set_tex_vec({{1, 0, 0, poly.tex_vec(0).w}, {0, 0, 1, poly.tex_vec(1).w}});
    } else {
        poly.set_tex_vec({{1, 0, 0, poly.tex_vec(0).w}, {0, 1, 0, poly.tex_vec(1).w}});
    }

    glm::vec4 tex_vec[2] = {poly.tex_vec(0), poly.tex_vec(1)};
	
    if (sum > eps) {
        for (unsigned j = 0; j < 3; j++) {
            tex_vec[0][j] *= mag_u;
            tex_vec[1][j] *= mag_v;
        }
    }

    glm::vec3 axis = glm::cross(glm::vec3(tex_vec[0]), glm::vec3(tex_vec[1]));
    axis = glm::normalize(axis);

    glm::mat4 rot_matrix(1);
    rot_matrix = glm::rotate(rot_matrix, poly.tex_angle(), axis);

    tex_vec[0] = rot_matrix * tex_vec[0];
    tex_vec[1] = rot_matrix * tex_vec[1];

    poly.set_tex_vec(tex_vec);

    unsigned num_points = poly.num_points();
    for (unsigned j = 0; j < num_points; j++) {
        glm::vec2 uv = {glm::dot(poly.vtx_pos(j), glm::vec3(tex_vec[0])) + tex_vec[0].w,
                        glm::dot(poly.vtx_pos(j), glm::vec3(tex_vec[1])) + tex_vec[1].w};
        uv *= tex_scale;
        poly.SetVertex(j, poly.vtx_pos(j), poly.vtx_normal(j), uv);
    }
}

std::vector<bsp::Brush> bsp::Brush::CSGUnion(const std::vector<Brush>::const_iterator brushes_beg,
                                             const std::vector<Brush>::const_iterator brushes_end,
                                             float eps) {
    std::vector<bsp::Brush> clipped_brushes = { brushes_beg, brushes_end };

    for (unsigned i = 0; i < clipped_brushes.size(); i++) {
        bool clip_on_plane = false;
        for (unsigned j = 0; j < clipped_brushes.size(); j++) {
            if (i != j) {
                clipped_brushes[i].ClipToBrush(*(brushes_beg + j), clip_on_plane, eps);
            } else {
                clip_on_plane = true;
            }
        }
    }

    return clipped_brushes;
}
