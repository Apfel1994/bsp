#pragma once

#include "Plane.h"
#include "Poly.h"

namespace bsp {
    class Node;
    
    class Portal {
        Poly poly_;
        Plane plane_;
        Node *front_node_, *back_node_;
    public:
        inline Portal(const Poly &poly, const Plane &plane, Node *front, Node *back);
        inline Portal(Poly &&poly, const Plane &plane, Node *front, Node *back);

        const Poly &poly() const { return poly_; }
        const Plane &plane() const { return plane_; }

        Node *front_node() const { return front_node_; }
        Node *back_node() const { return back_node_; }

        void set_front_node(Node *node) { front_node_ = node; }
        void set_back_node(Node *node) { back_node_ = node; }

        Portal ClipToAntiPenumbra(const std::vector<Plane> &clip_planes, const Portal &gen_portal, float eps) const;
    
        static std::vector<Plane> ConstructClipPlanes(const Portal &source, const Portal &target, float eps);

        static bool IsEqual(const Portal &p1, const Portal &p2, float eps) {
            return Plane::IsEqual(p1.plane_, p2.plane_, eps) && Poly::IsEqual(p1.poly_, p2.poly_, eps);
        }
    };
}

bsp::Portal::Portal(const Poly &poly, const Plane &plane, Node *front, Node *back)
    : poly_(poly), plane_(plane), front_node_(front), back_node_(back) {}

bsp::Portal::Portal(Poly &&poly, const Plane &plane, Node *front, Node *back)
    : poly_(std::move(poly)), plane_(plane), front_node_(front), back_node_(back) {}

