#pragma once

#include <cassert>

#include <glm/geometric.hpp>

#include "Poly.h"

namespace bsp {
    enum eClassifyResult { Front, Back, OnPlane, TouchFront, TouchBack, Spanning };
    
    struct Plane {
        glm::vec3 n;
        float     d;

        Plane(const glm::vec3 &n, float d) : n(glm::normalize(n)), d(d) {}

        bool is_valid() const { return !(glm::isnan(n.x) || glm::isnan(n.y) || glm::isnan(n.z) || glm::isnan(d)); }

        inline eClassifyResult ClassifyPoint(const glm::vec3 &p, float eps) const;
        inline eClassifyResult ClassifyPoly(const Poly &poly, float eps) const;

        static inline bool IsEqual(const Plane &pl1, const Plane &pl2, float eps);
    };
    
    inline bool GetIntersection(const Plane &pl1, const Plane &pl2, const Plane &pl3, float eps, glm::vec3 &out_p);

}

bsp::eClassifyResult bsp::Plane::ClassifyPoint(const glm::vec3 &p, float eps) const {
    assert(is_valid());
    float res = glm::dot(n, p) + d;
    if (res > eps) {
        return Front;
    } else if (res < -eps) {
        return Back;
    } else {
        return OnPlane;
    }
}

bsp::eClassifyResult bsp::Plane::ClassifyPoly(const Poly &poly, float eps) const {
    assert(is_valid());
    unsigned num_front = 0, num_back = 0, num_on_plane = 0;
    unsigned num_points = poly.num_points();
    for (unsigned int i = 0; i < num_points; i++) {
        eClassifyResult result = ClassifyPoint(poly.vtx_pos(i), eps);
        if (result == Front) {
            num_front++;
        } else if (result == Back) {
            num_back++;
        } else {
            num_on_plane++;
        }
    }
    if (num_front == num_points) {
        return Front;
    } else if (num_back == num_points) {
        return Back;
    } else if (num_on_plane == num_points) {
        return OnPlane;
    } else if (num_front + num_on_plane == num_points) {
        return TouchFront;
    } else if (num_back + num_on_plane == num_points) {
        return TouchBack;
    }
    return Spanning;
}

bool bsp::Plane::IsEqual(const Plane &pl1, const Plane &pl2, float eps) {
    assert(pl1.is_valid() && pl2.is_valid());
    if (glm::abs(pl1.d - pl2.d) > eps) return false;
    for (int i = 0; i < 3; i++) {
        if (glm::abs(pl1.n[i] - pl2.n[i]) > eps) return false;
    }
    return true;
}

bool bsp::GetIntersection(const Plane &pl1, const Plane &pl2, const Plane &pl3, float eps, glm::vec3 &out_p) {
    assert(pl1.is_valid() && pl2.is_valid() && pl3.is_valid());
    glm::vec3 cross_2_3 = glm::cross(pl2.n, pl3.n);

    float denom = glm::dot(pl1.n, cross_2_3);

    if (glm::abs(denom) < eps) {
        return false;
    }
    denom = 1 / denom;

    glm::vec3 cross_3_1 = glm::cross(pl3.n, pl1.n);
    glm::vec3 cross_1_2 = glm::cross(pl1.n, pl2.n);

    out_p = (-pl1.d * cross_2_3 - pl2.d * cross_3_1 - pl3.d * cross_1_2) * denom;
    return true;
}

