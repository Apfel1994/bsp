#pragma once

#include <cstdint>

#include <vector>

#include "Plane.h"

namespace bsp {
    class Brush {
        VertexPool *pool_;

        glm::vec3 bbox_min_, bbox_max_;

        std::vector<Plane> planes_;
        std::vector<Poly> polys_;

        uint32_t flags_;
    public:
        struct Side {
            glm::vec3 p1, p2, p3;

            Side(const glm::vec3 &_p1, const glm::vec3 &_p2, const glm::vec3 &_p3)
                : p1(_p1), p2(_p2), p3(_p3) {}
        };
        Brush(VertexPool *pool, const std::vector<Side> &sides);

        const glm::vec3 &bbox_min() const { return bbox_min_; }
        const glm::vec3 &bbox_max() const { return bbox_max_; }
    
        unsigned num_sides() const { return (unsigned)planes_.size(); }
    
        const Plane &plane(unsigned i) const { return planes_[i]; }
        const Poly &poly(unsigned i) const { return polys_[i]; }
    
        void InitPolys(float eps);
        void UpdateUVs(float scale);

        void SetTexProperties(unsigned i, unsigned mat_id, const glm::vec4 (&tex_vec)[2], float angle);
        void SetSmoothingGroup(unsigned i, unsigned val);

        void ClipToBrush(const Brush &brush, bool clip_on_plane, float eps);

        static void UpdatePolyUVs(Poly &poly, const glm::vec3 &n, float tex_scale);
    
        static std::vector<Brush> CSGUnion(const std::vector<Brush>::const_iterator brushes_beg,
                                           const std::vector<Brush>::const_iterator brushes_end,
                                           float eps);
    };
}
